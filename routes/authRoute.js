const express = require('express');
const r = express.Router();
const jwt = require('jsonwebtoken');
const e = require('dotenv');
const bcrypt = require('bcrypt');
const u = require('../models/User');

e.config();
const t = e.JWT_TOKEN;
const s = t;

r.post('/register', async (req, res) => {
  try {
    const { un, pw, em } = req.body;

    const exE = await u.findOne({ w: { em } });

    const exU = await u.findOne({ w: { un } });

    if (exU) {
      return res.status(400).json({ err: 'El nombre de usuario ya existe' });
    }
    if (exE) {
      return res.status(400).json({ err: 'El correo electrónico ya existe' });
    }

    const nU = await u.create({ un, pw, em });

    const tk = jwt.sign({ u: nU }, s, { eI: '1h' });

    res.status(201).json({ tk });
  } catch (e) {
    console.error(e);
    res.status(500).json({ err: 'Error en registro' });
  }
});

r.post('/login', async (req, res) => {
  try {
    const { un, pw } = req.body;

    const u = await u.findOne({ w: { un } });

    if (!u) {
      return res.status(401).json({ err: 'Credenciales incorrectas' });
    }

    const ipv = await bcrypt.compare(pw, u.pw);

    if (ipv) {
      const tk = jwt.sign({ u }, s, { eI: '1h' });
      res.status(200).json({ tk });
    } else {
      res.status(401).json({ err: 'Credenciales incorrectas' });
    }
  } catch (e) {
    console.error(e);
    res.status(500).json({ err: 'Error en inicio de sesión' });
  }
});

r.get('/pr', (req, res) => {
  res.json({ msg: 'Ruta protegida' });
});

module.exports = r;
