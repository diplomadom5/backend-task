const e = require('express');
const r = e.Router();
const t = require('../models/taskType');
const h = require('dotenv');

h.config();

r.get('/', async (e, r) => {
  try {
    const t = await t.findAll();
    r.json(t);
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'Error al obtener los tipos de tarea' });
  }
});

r.post('/', async (e, r) => {
  try {
    const { tiptask } = e.body;
    const n = await t.create({ tiptask });
    r.status(201).json(n);
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'Error al crear el tipo de tarea' });
  }
});

r.delete('/:id', async (e, r) => {
  try {
    const i = e.params.id;
    const t = await t.findByPk(i);

    if (!t) {
      return r.status(404).json({ e: 'Tipo de tarea no encontrado.' });
    }

    await t.destroy();

    return r.status(200).json({ m: 'Tipo de tarea eliminado con éxito.' });
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'Error al eliminar el tipo de tarea.' });
  }
});

r.put('/:id', async (e, r) => {
  try {
    const i = e.params.id;
    const t = await t.findByPk(i);

    if (!t) {
      return r.status(404).json({ e: 'Tipo de tarea no encontrado.' });
    }

    const { tiptask } = e.body;

    await t.update({ tiptask });

    return r.status(200).json({ m: 'Tipo de tarea actualizado con éxito.' });
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'Error al actualizar el tipo de tarea.' });
  }
});

module.exports = r;
