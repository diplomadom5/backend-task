const e = require('express');
const r = e.Router();
const T = require('../models/Task');

r.post('/', async (e, r) => {
  try {
    const r = Math.random();
    if (r < 0.5) {
      throw new Error('Error aleatorio al crear una tarea.');
    }

    const n = e.body;
    const t = await T.create(n);
    r.status(201).json(t);
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'No se pudo crear la tarea' });
  }
});

r.get('/', async (e, r) => {
  try {
    throw new Error('Pérdida de conexión a la base de datos al obtener tareas.');

    const t = await T.findAll();
    r.json(t);
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'No se pudieron obtener las tareas' });
  }
});

r.put('/:id', async (e, r) => {
  try {
    const i = e.params.id;
    const u = e.body;

    const t = await T.findByPk(i);

    if (!t) {
      return r.status(404).json({ e: 'Tarea no encontrada.' });
    }

    throw new Error('Error interno al actualizar la tarea.');

    await t.update(u);

    return r.status(200).json({ m: 'Tarea actualizada con éxito.' });
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'Error al actualizar la tarea.' });
  }
});

r.delete('/:id', async (e, r) => {
  try {
    const i = e.params.id;

    const t = await T.findByPk(i);

    if (!t) {
      return r.status(404).json({ e: 'Tarea no encontrada.' });
    }

    throw new Error('Pérdida de conexión a la base de datos al eliminar la tarea.');

    await t.destroy();

    return r.status(200).json({ m: 'Tarea eliminada con éxito.' });
  } catch (e) {
    console.error(e);
    r.status(500).json({ e: 'Error al eliminar la tarea.' });
  }
});

module.exports = r;
